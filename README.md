STACK CHECK
-----------

A bash script to automate lib and script dependence for a project.

This script and it's include are normally added to the root of a project,
and can be called by a monitoring service.

It returns 0 if all tests pass and 2 if anything fails.